Material can be withheld from the public for the following reasons:

* The material contains sensitive, identifying information, including Social Security Numbers.
* The material pertains to sensitive employment actions, including discipline. 
* The material is part of the deliberative process or is a draft of a to-be-public document.

## Residential Exemptions

Residential exemptions identify individual people based on age, disability status, veterans status, and income. This is a lot of personal information. We have decided not to publish individual exemptions in order to protect the privacy of taxpayers.

## RPIE Data

Some RPIE data individually identifies and reveals the income and profits of commercial properties. These fields have been withheld from public data due to privacy concerns. 

## Notes Repository

The CCAO maintains a [repository with notes from meetings and internal resources](https://gitlab.com/ccao-data-science---modeling/documentation/notes). This is not made public since most of the notes are part of the deliberative process and would be nearly useless to members of the public.
