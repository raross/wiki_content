[Home](Home)

**The Data Science Department**

 * [Handbook](Handbook/Handbook)
 * [Mission, Vision, and Values](Handbook/Mission Vision Values)
 * [Resources](Handbook/Resources)
 * [Glossary](Handbook/Glossary)

**People**

 * [Onboarding](People/Onboarding)
 * [External Developer Engagement Program](People/Contributing)
 * [VPN Dos and Don'ts](People/VPN)

**Standard Operating Procedures (SOPs)**

 * [Sales Ratio Studies](SOPs/Sales Ratio Studies)
 * [Desk Review](SOPs/Desk Review)
 * [Open Data](SOPs/Open Data)

**Residential**

 * [Single and Multi-Family Residential Model](https://gitlab.com/ccao-data-science---modeling/models/ccao_res_avm)
 * [Condominium Model](https://gitlab.com/ccao-data-science---modeling/models/ccao_condo_avm)
 * [Residential Exemptions](Residential/Residential Exemptions)
 * [Home Improvement Exemptions (288s)](Residential/Home Improvement Exemptions)

**Commercial and RPIE**

 * [Commercial Apartments Model](https://gitlab.com/ccao-data-science---modeling/models/commercial-apartments-automated-valuation-model)
 * [RPIE Overview](RPIE/RPIE Overview)
 * [RPIE Landing Page Content](RPIE/Overview)
 * [RPIE FAQs](RPIE/FAQs)
 * [RPIE Privacy Policy](RPIE/Privacy Policy)
 * [RPIE Details](RPIE/What To Expect)

**Data Documentation**

 * [AssessR R Package Documentation](https://ccao-data-science---modeling.gitlab.io/packages/assessr/reference/)
 * [CCAO R Package Documentation](https://ccao-data-science---modeling.gitlab.io/packages/ccao/reference/)
 * [(DEPRECATED) SQL Database Guide](Data/SQL Database Guide)
 * [Property Class Definitions](Data/class-definitions.pdf)
 * [Township Definitions](Data/Townships)
 * [Appeal Reason Codes](https://prodassets.cookcountyassessor.com/s3fs-public/form_documents/reasoncodes.pdf)
 * [CDU Codes](Data/CDU Codes)
 * [Accessing iasWorld Oracle Database](Data/Accessing iasWorld Oracle Database)
