Various CCAO services are only available while connected the CCAO's network using Cisco AnyConnect Secure Mobility Client&trade;. Below is a list of services employees may need to use and whether or not they'll need to be connected to the VPN in order to use those services.

| Service | VPN Required | 
| ------- |:---:|
| [Web Clock](https://www.cookcountyil.gov/cct) | Sometimes Yes, Sometimes No :shrug: |
| [Intranet](http://intranet/) | ***Must*** be Connected |
| [SQL Server](10.129.122.31) | ***Must*** be Connected |
| [iasWorld Backend](iptsdgvorapri02.ccounty.com:1521) | ***Must*** be Connected |
| Shared Drives | ***Must*** be Connected |
| [Shiny Server](https://datascience.cookcountyassessor.com/shiny/) | ***Must*** be Connected |
| [RStudio Server](https://datascience.cookcountyassessor.com/rstudio/)| ***Must*** be Connected |
| [iasWorld Frontend (Test)](http://iptsweb-tst.ccounty.com/) | ***Must*** be Connected |
| [iasWorld Frontend (Prod)](https://iptsweb.ccounty.com/) | ***Must*** be Connected |
| OneDrive | ***Cannot*** be Connected |
| Teams | Connection Agnostic |
| Outlook | Connection Agnostic |
| [Webmail (from home)](https://webmail.cookcountyassessor.com/owa/#path=/mail) | Connection Agnostic |
| [Webmail (from guest network @ CCAO)](https://webmail.cookcountyassessor.com/owa/#path=/mail) | ***Cannot*** be Connected |
| [Cook Viewer](https://maps.cookcountyil.gov/cookviewer/) | ***Cannot*** be Connected |
