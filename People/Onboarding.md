In order to better track the progress of onboarding new employees, interns, and collaborators, we've moved the onboarding process to the [Employment repository](https://gitlab.com/ccao-data-science---modeling/people/employment).

* [Pre-onboarding for new employees and interns](https://gitlab.com/ccao-data-science---modeling/people/employment/-/blob/master/.gitlab/issue_templates/pre-onboarding.md)
* [Onboarding for new employees and interns](https://gitlab.com/ccao-data-science---modeling/people/employment/-/blob/master/.gitlab/issue_templates/onboarding-internal.md)
* [Onboarding for returning employees and interns](https://gitlab.com/ccao-data-science---modeling/people/employment/-/blob/master/.gitlab/issue_templates/onboarding-returning.md)
* [Onboarding for external contributors](https://gitlab.com/ccao-data-science---modeling/people/employment/-/blob/master/.gitlab/issue_templates/onboarding-external.md)
* [Offboarding for interns](https://gitlab.com/ccao-data-science---modeling/people/employment/-/blob/master/.gitlab/issue_templates/offboarding.md)
